Role Name
=========

CVMFS client

https://cvmfs.readthedocs.io/en/stable/

Requirements
------------

None

Role Variables
--------------

None

Dependencies
------------

None

Example Playbook
----------------

    - hosts: desktop
      roles:
         - dietrichliko.cvmfs_client

License
-------

MIT

Author Information
------------------

Dietrich.Liko@oeaw.ac.at
